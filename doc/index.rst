ewoksid01 |version|
===================

*ewoksid01* provides data processing workflows for ID01.

*ewoksid01* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_
and the ID01 staff of the `European Synchrotron <https://www.esrf.eu/>`_.

Documentation
-------------

.. toctree::
    :maxdepth: 2

    api
